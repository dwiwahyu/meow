from mysql.connector import connect


class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='kucing',
                              user='root',
                              password='')
        except Exception as e:
            print(e)

 
    def showCatById(self, **params):
        try:
            id = params['id']
            crud_query = '''select * from daftar where id = {0};'''.format(id)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            dat = cursor.fetchone()
            return dat
            #for row in dat:
             #   print(row)
        except Exception as e:
            print(e)

    def insertCat(self, **params):
        try:
            column = ', '.join(list(params['values'].keys()))
            values = tuple(list(params['values'].values()))
            crud_query = '''insert into daftar ({0}) values {1};'''.format(
                column, values)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def updateCatById(self, **params):
        try:
            id = params['id']
            values = self.restructureParams(**params['values'])
            crud_query = '''update daftar set {0} where id = {1};'''.format(
                values, id)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def deleteCatById(self, **params):
        try:
            id = params['id']
            crud_query = '''delete from daftar where id = {0};'''.format(
                id)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(
            item[0], item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
