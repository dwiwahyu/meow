from customers import database
from flask import Flask, jsonify, request
import json

app = Flask(__name__)


@app.route("/")
def main():
    return "Welcome!"

@

@app.route("/cat", methods=["POST"])
def showCat():
    params = request.json
    dbresult = mysqldb.showUserById(**params)
    catss = {
        "id" : dbresult[0],
        "name" : dbresult[1],
        "gender" : dbresult[2],
        "ras" : dbresult[3],
        "price" : dbresult[4]            
    }
        
    return jsonify(catss) 

@app.route("/catInsert", methods=["POST"])
def showCat():
    params = request.json
    dbresult = mysqldb.insertCat(**params)
    catss = {
        "id" : dbresult[0],
        "name" : dbresult[1],
        "gender" : dbresult[2],
        "ras" : dbresult[3],
        "price" : dbresult[4]            
    }
        
    return jsonify(catss)  

@app.route("/catupdate", methods=["POST"])
def showCat():
    params = request.json
    dbresult = mysqldb.updateCatById(**params)
    catss = {
        "id" : dbresult[0],
        "name" : dbresult[1],
        "gender" : dbresult[2],
        "ras" : dbresult[3],
        "price" : dbresult[4]            
    }
        
    return jsonify(catss) 

@app.route("/catdelete", methods=["POST"])
def showCat():
    params = request.json
    dbresult = mysqldb.deleteCatById(**params)
    catss = {
        "id" : dbresult[0],
        "name" : dbresult[1],
        "gender" : dbresult[2],
        "ras" : dbresult[3],
        "price" : dbresult[4]            
    }
        
    return jsonify(catss) 



  

if __name__ == "__main__":
    mysqldb = database()
    if mysqldb.db.is_connected():
        print('Connected to MySQL database')
    
    app.run(debug=True)
    
    if mysqldb.db is not None and mysqldb.db.is_connected():
        mysqldb.db.close()